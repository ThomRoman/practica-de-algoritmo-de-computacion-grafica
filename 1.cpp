#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <math.h>

//puntos de control de la superficie de Bezier

// con estos puntos los puntos de las coordenas de cada superficie
// al unirse estos puntos se forma una superficie

// estas coordenas se cuentan de arriba hacia abajo en el grafico de la compilacion
GLfloat ctrlpoints[5][5][3] = {
    // esta es la primera columna
    {
        // estas son las coordenadas de las filas   
        {-2.0, -2.0, 0.5}, // fila 1
        {-1.0, -2.0, -0.4},  // fila 2
        {0.0, -2.0, -0.5},  // fila 3
        {1.0, -2.0, -0.4},  // fila 4
        {2.0, -2.0, 0.5}  // fila 5
    },
    // esta es la segunda columna
    {
        {-2.0, -1.0, 0.5}, 
        {-1.0, -1.0, 0.5}, 
        {0.0, -1.0, 0.75}, 
        {1.0, -1.0, 0.5}, 
        {2.0, -1.0, 0.25}
    },
    // esta es la tercera columna
    {
        {-2.0, 0.0, 0.6}, 
        {-1.0, 0.0, 1.0}, 
        {0.0, 0.0, 1.5}, 
        {1.0, 0.0, 1.0}, 
        {2.0, 0.0, 0.26}
    },
    //esta es la cuarta columna
    {
        {-2.0, 1.0, 0.5}, 
        {-1.0, 1.0, 1.0}, 
        {0.0, 1.0, 1.25}, 
        {1.0, 1.0, 1.0}, 
        {2.0, 1.0, 0.25}
    }, 
    //esta es la quinta columna
    {
        {-2.0, 2.0, 1.0}, 
        {-1.0, 2.0, 0.5}, 
        {0.0, 2.0, 0.25}, 
        {1.0, 2.0, 0.5}, 
        {2.0, 2.0, 1.0}
    }
};

//propiedades del material
GLfloat mat_ambient[] = {0.0f, 0.05f, 0.06f, 1.0f};
GLfloat mat_diffuse[] = {0.0f, 0.50980392f, 0.50980392f, 1.0f};
GLfloat mat_specular[] = {0.90196078f, 0.90196078f, 0.90196078f, 1.0f};
GLfloat rotx = 0.0;
GLfloat roty = 0.0;
GLfloat rotz = 0.0;

// Hay algunas funciones que podemos usar para que los objetos desplegados es pantalla se vean mejor, para esto crearemos una función que llamaremos void initlights()
void initlights(void){
    //propiedades de la fuente de luz
    GLfloat ambient[] = {0.2f, 0.2f, 0.2f, 1.0f};
    GLfloat diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f}; 
    GLfloat position[] = {0.0f, 0.0f, 1.0f, 0.0f};

    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient); // Reflexion ambient: representa el color que va a tener la zona oscura que tendra el material 
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse); //Reflexion diffuse: es el colo del objeto (GL_DIFFUSE = (R,G,B,1.0)). 
    glLightfv(GL_LIGHT0, GL_POSITION, position);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
}
//dibuja los puntos de control de la superficie
void DrawPoints(void)
{
    int i, j;
    glPushAttrib(GL_LIGHTING_BIT); // esta funcion me permite insertar un valor en una pila el estado en forma de bit
    glDisable(GL_LIGHTING);

    //cambio el tamaño del pexels para hacerlo mas visible
    glPointSize(4.0f); //dibujo todos los puntos de control de la superficie
    glColor3f(0.0, 1.0, 1.0);
    glBegin(GL_POINTS);

    // aqui dibuja los puntos de control
    for (j = 0; j < 5; j++) // en esta parte estan las columnas
        for (i = 0; i < 5; i++) // en esta parte estan las filas
            glVertex3fv(ctrlpoints[j][i]); // en esta parte se coloca la coordena
    glEnd();
    glColor3f(0.0, 0.25, 0.5);

    // se inicializa para el dibujo de lineas
    glBegin(GL_LINES);
    for (j = 0; j < 4; j++)
        for (i = 0; i < 4; i++)
        {
            glVertex3fv(ctrlpoints[j][i]);
            glVertex3fv(ctrlpoints[j][i + 1]);
        }
    for (i = 0; i < 4; i++)
        for (j = 0; j < 4; j++)
        {
            glVertex3fv(ctrlpoints[j][i]);
            glVertex3fv(ctrlpoints[j + 1][i]);
        }
    glEnd();
    glPopAttrib(); // lo retira de la pila
}
//dibuja la superficie
void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW_MATRIX);
    glLoadIdentity();
    glTranslated(0.0, 0.0, -12.0);
    glRotatef(rotx, 1.0, 0.0, 0.0);
    glRotatef(roty, 0.0, 1.0, 0.0);
    glRotatef(rotz, 0.0, 0.0, 1.0); 
    
    //material para la superficie de Bezier
    glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialf(GL_FRONT, GL_SHININESS, 128.0f);

    glEvalMesh2(GL_FILL, 0, 20, 0, 20); //me dibuja la superficie en modo shaded y conglEvalMesh2(GL_LINE, 0, 20, 0, 20)en modo wireframe (solo lineas)
    DrawPoints();
    glFlush();
}
void init(void)
{
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glEnable(GL_DEPTH_TEST);
    
    //aqui se le especifica el número de puntos de control de la superficie, como asi tambien el arreglo donde estan definidos estos puntos. Con 
    //GL_MAP2_VERTEX_3(Tipo del punto de control) se le indica que se le van a pasar coordenadas de los vertices, de los puntos de control (x,y,z). 
    glMap2f(GL_MAP2_VERTEX_3, 0, 1, 3, 5, 0, 1, 15, 5, &ctrlpoints[0][0][0]);
    /*
    0 = el valor mas bajo del parametro u.
    1 = el valor mas alto del parametro u.
    3 = es el numero de datos por punto de control, en este caso 3 ya que tenemos x,y,z.
    5 = dimencion en la direccion u, en este caso 5.
    0 = el valor mas chico de v
    1 = el valor mas grande de v.
    15 = distancia entre los puntos en ctrlpoints, en este caso 3*5 = 15.
    5 =  dimencion en la direccion v.
    &ctrlpoints[0][0][0]=es el array que contiene los puntos de control
    */

    //activa el evaluador
    glEnable(GL_MAP2_VERTEX_3); //se le indica que se le van a pasar coordenadas de los vertices
    glEnable(GL_AUTO_NORMAL); //le indicamos que me calcule en forma automatica las normales, para utilizar el modo de dibujo shaded
    glEnable(GL_NORMALIZE); // hace que estas normales esten normalizadas

    //me crea una grilla de 400 puntos (20x20)
    // Esta función me crea una grilla 2D, es usada con glMap2f() para evaluar eficientemente y crear la malla con las coordenadas de la superficie.
    glMapGrid2f(20, 0.0, 1.0, 20, 0.0, 1.0); //me crea una grilla con 20 puntos que estancomprendidos entre 0.0 y 1.0, eso para u y v
    initlights();
}

// funcion encargado de la remodelacion del wireframe
void reshape(int w, int h)
{
    glViewport(0, 0, (GLsizei)w, (GLsizei)h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(30, (GLfloat)w / (GLfloat)h, 1.0, 50.0);
    glMatrixMode(GL_MODELVIEW);
}


void keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
    case '1':
        rotx = rotx + 10.0f;
        glutPostRedisplay(); // que fuerza a que se ejecute la función asignada a glutDisplayFunc(),
        break;
    case '2':
        rotx = rotx - 10.0f;
        glutPostRedisplay(); // que fuerza a que se ejecute la función asignada a glutDisplayFunc(),
        break;
    case '3':
        roty = roty + 10.0f;
        glutPostRedisplay(); // que fuerza a que se ejecute la función asignada a glutDisplayFunc(),
        break;
    case '4':
        roty = roty - 10.0f;
        glutPostRedisplay(); // que fuerza a que se ejecute la función asignada a glutDisplayFunc(),
        break;
    case '5':
        rotz = rotz + 10.0f;
        glutPostRedisplay(); // que fuerza a que se ejecute la función asignada a glutDisplayFunc(),
        break;
    case '6':
        rotz = rotz - 10.0f;
        glutPostRedisplay(); // que fuerza a que se ejecute la función asignada a glutDisplayFunc(),
        break;
    case 27:
        exit(0);
        break;
    }
}
int main(int argc, char **argv)
{
    glutInit(&argc, argv); //Lo primero que debemos mandar llamar en un proyecto con lal ibreŕıa GLUT
    // &argc - referencia al espacio de memoria que es la cantidad de elementos que se le colocan en la terminal
    // argv lista de elementos (banderas) con la cual la funcion main se invoca (-lglu32 -lglut32 -lopengl32 -lstdc++)

    // estas dos variables son importantes cuando se hacen programas de linea de comando que acepten argumentos como parte de la invocacion del programa

    
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH); // nos permite definir el modo en que semostrara la ventana principal de nuestro proyecto
    // Las constantes predefinidas para el color son
    // GLUTRGBA or GLUTRGB - selecciona una ventana RGBA(Red,Green, Blue, Alpha). El cual es el color por defecto.
    // GLUTINDEX - selecciona un modo indexado de colores
    
    // El modo mostrar tambien nos permite seleccionar una ventana de un ́unico o doble buffer, las constantes son
    // GLUTSINGLE - Un unico buffer.
    // GLUTDOUBLE - Una ventana de doble buffer. Nos permite teneranimaciones suaves

    // Ademas puedes especificar si tu ventana tendr ́a un conjunto de buffersparticular. Los ḿas comunes son
    // GLUTDEPTH - El buffer profundo
    // GLUTSTENCIL - El buffer plantilla.
    // GLUTACCUM - El buffer de acumulaci ́on

    glutInitWindowSize(300, 300); //nos permite definir el tamãno de la ventanaprincipal de nuestro proyecto.
    glutInitWindowPosition(100, 100); // nos permite definir donde se encontrara laesquina superior izquierda de la ventana principal de nuestro proyecto
    glutCreateWindow("Superficie de Bazier");
    init();
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutDisplayFunc(display);
    glutMainLoop();
    return 0;
}

// g++ -m32 -Wall -o 1.out 1.cpp -L"C:\MinGW\lib" -lglu32 -lglut32 -lopengl32 -lstdc++