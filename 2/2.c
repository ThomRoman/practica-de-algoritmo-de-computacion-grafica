//Practica B�sica: Transformaciones Geom�tricas
//Ejercicio 2 Ejercicios Propuestos
#include <stdlib.h>
#include <math.h>
// #include <iostream>
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

GLfloat movY = 0, movX = 0, rotar = 0, tam = 0.0, escX = 0.3, escY = 0.3, escZ = 0.1;

void principal()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	glTranslated(movX, movY, 0.0);
	glRotatef(rotar, 0.1, 0.1, 0.0);
	glScalef(escX, escY, escZ);

	glPushMatrix();

	glutWireSphere(tam, 50, 50);
	glPopMatrix();
	glFlush();
	glutSwapBuffers();
}

void movimiento(char keys, int x, int y)
{
	switch (keys)
	{ //MOVIMIENTO
	case GLUT_KEY_RIGHT:
		movX += 0.1;
		break;
	case GLUT_KEY_LEFT:
		movX -= 0.1;
		break;
	case GLUT_KEY_UP:
		movY += 0.1;
		break;
	case GLUT_KEY_DOWN:
		movY -= 0.1;
		break;
	}
	glutPostRedisplay();
}

void aumentar(char pres)
{
	switch (pres)
	{
	case 'a': //rotar esfera
		rotar -= 4.1;
		break;
	case 's': //rotar esfera
		rotar += 4.1;
		break;
	case 'j': //aumenta tama�o de la esfera
		tam += 0.3;
		break;
	case 'k': //reduce tama�o de la esfera
		tam -= 0.3;
		break;
	case 'u': //estirar en X
		escX += 0.1;
		break;
	case 'i': //estirar en Y
		escY += 0.1;
		break;
	case 'o': //estirar en Z
		escZ += 0.1;
		break;
	}
	glutPostRedisplay();
}

int main(int argc, char *argv[])
{
	printf("------------------INSTRUCCIONES DEL PROGRAMA------------------\n");
	printf("Observacion1: Recuerde tener desactivado el boton de Mayusculas\n");
	printf("Observacion2: Para que aparezca la figura presione la tecla 'j' e\n");
	printf("ira aumentando su tamaño y sera notorio en la pantalla\n");
	printf("--------------------------------------------------------------\n");
	printf("ESCALAMIENTO DE LA ESFERA\n");
	printf("	Tecla 'j' : Aumenta el tama�o de la esfera\n");
	printf("	Tecla 'k' : Reducir el tama�o de la esfera\n");
	printf("	Tecla 'u' : Estirar en el eje X\n");
	printf("	Tecla 'i' : Estirar en el eje Y\n");
	printf("	Tecla 'o' : Estirar en el eje Z\n");

	printf("	Tecla Flecha Derecha : Desplaza hacia la derecha la figura\n");
	printf("	Tecla Flecha Isquierda : Desplaza hacia la izquierda la figura\n");
	printf("	Tecla Flecha hacia Arriba : Desplaza hacia arriba la figura\n");
	printf("	Tecla Flecha hacia Abajo : Desplaza hacia abajo la figura\n");

	printf("ROTACION DE LA ESFERA\n");
	printf("	Tecla 'a' : Rotar en sentido horario\n");
	printf("	Tecla 's' : Rotar en sentido antihorario\n");
	glutInit(&argc, argv); // Inicializa GLUT
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(800, 800);
	glutInitWindowPosition(450, 150);
	glutCreateWindow("Rotaciones y Escalamiento usando el teclado"); // Crea un titulo
	glEnable(GL_DEPTH_TEST);
	glutDisplayFunc(principal);
	glutSpecialFunc(movimiento); //Traslacion
	glutKeyboardFunc(aumentar);	 //rotacion y escalacion
	glutMainLoop();				 // permite que el proceso se repita hasta que el usuario cierre pantalla
	return 0;
}

// gcc -m32 -Wall -o 2.out 2.c -L"C:\MinGW\lib" -lglu32 -lglut32 -lopengl32 -lstdc++